// Global app controller
import axios from 'axios';
import Search from './models/Search'
import * as searchView from './views/searchView';
import * as recipeView from './views/recipeView';
import {elements,renderLoader,clearLoader} from './views/base';
import Recipe from './models/Recipe';
import List from './models/list';
import * as listView from './views/listView';
import Likes from './models/likes';
import * as likesView from './views/likesView';

/* Global state of the app */
// - Search object
// - Current recipe object
// - Shopping list objecr
// - Liked recipes
const state = {};

///////////////////////////////////////////////////////////////////////////////////
// Search Controller
const controlSearch = async () => {
    // get the query from the view
    const query = searchView.getInput();

    if(query){
        // Create a new seach object
        state.search = new Search(query);
    }

    // Prepare UI fo results
    searchView.clearInput();
    searchView.clearResults();
    renderLoader(elements.searchResult);

    try{
        // Search for recipes
        await state.search.getResults();

        // Render results on the UI
        clearLoader();
        searchView.renderResults(state.search.result);
    }catch(error) {
        alert(`Error processing the request`);
        clearLoader();
    }
}

elements.searchForm.addEventListener('submit', e => {
    e.preventDefault();
    controlSearch()
});

elements.searchResPages.addEventListener('click',e => {
    const btn = e.target.closest('.btn-inline');
     if(btn){
         const goToPage = parseInt(btn.dataset.goto,10);
         searchView.clearResults();
         searchView.renderResults(state.search.result, goToPage);
     }
});

///////////////////////////////////////////////////////////////////////////////
// Recipe Controller

const controlRecipe = async ()  => {
    const id = parseInt(window.location.hash.replace('#',''));

    if(id) {
        // Prepare UI for changes
        renderLoader(elements.rr);
        

        //Create new Recipe object
        state.recipe = new Recipe(id);
        
        //TESTING
        window.r = state.recipe;

        try{
            //Get Recipe data
            await state.recipe.getRecipe();
            recipeView.clearRecipe();
            clearLoader();
            
            
            //calculate Servings and time
            state.recipe.calcTime();
            state.recipe.calcServing();
            state.recipe.parseIngredients();
            
            //Render Recipe
            recipeView.renderRecipe(
                state.recipe,
                state.likes.isLiked(id)
                );
            if(state.search) searchView.highlightSelect(id);

        }catch (error) {
            alert(`${error}`);
        }
    }
}

// window.addEventListener('hashchange', controlRecipe);
// window.addEventListener('load',controlRecipe);

['hashchange','load'].forEach(event => window.addEventListener(event, controlRecipe));

//List Controller 
const controlList = () => {
    //Create a new list IF there is none yet
    if(!state.list) state.list = new List();
    //Add each ingredients in the list
    state.recipe.ingredients.forEach(el => {
        const item = state.list.addItem(el.count,el.unit,el.ingredient);
        listView.renderItem(item);
    });
}
//Handle delete and update list events
elements.shopping.addEventListener('click', e => {
    const id = e.target.closest('.shopping__item').dataset.itemid;
    
    //Handle the delete button
    if(e.target.matches('.shopping__delete, .shopping__delete *')){
        //Delete from state
        state.list.deleteItem(id);
        listView.deleteItem(id);
    }else if (e.target.matches('.shopping__count-value')) {
        const val = parseFloat(e.target.value);
        state.list.updateCount(id, val);
    }
})

//TESTING


// Likes controller 
const controllerLike = () => {
    if(!state.likes) state.likes = new Likes();
    const currentID = state.recipe.id;
    if(!state.likes.isLiked(currentID)) {
        //Add like to the state
        const newLike = state.likes.addLike(
            currentID,
            state.recipe.title,
            state.recipe.author,
            state.recipe.img
            );
        //Toggle the like button
        likesView.toggleLikedBtn(true);
        
        //AAdd like to the UI list
        likesView.renderLike(newLike);
    }else {
        //Remove like from the state
        state.likes.deleteLike(currentID);
        
        // Toggle the like button 
        likesView.toggleLikedBtn(false);
        
        //Remove like from UI list
        likesView.deleteLike(currentID);
    }
    likesView.toggleLikeMenu(state.likes.getNumLikes());
}

//Restore recipes on page load
window.addEventListener('load', () => {
    state.likes = new Likes();
    // Restore items
    state.likes.readStorage();
    //Toggle like menu
    likesView.toggleLikeMenu(state.likes.getNumLikes());
    // Render the likes
    state.likes.likes.forEach(el => likesView.renderLike(el));
});

// Handling recipe button clicks
elements.rr.addEventListener('click',e => {
    if(e.target.matches('.btn-decrease, .btn-decrease *')){
        //Decrease button is clicked
        if(state.recipe.servings > 1){
            state.recipe.updateServings('dec');
            recipeView.updateServingsIngredients(state.recipe);
        }
    }else if(e.target.matches('.btn-increase, .btn-increase *')){
        //increase button is clicked
        if(state.recipe.servings >= 1){
            state.recipe.updateServings('inc');
            recipeView.updateServingsIngredients(state.recipe);
        }
    }else if(e.target.matches('.recipe__btn--add, .recipe__btn--add *')){
        controlList();
    }else if(e.target.matches('.recipe__love, .recipe__love *')){
        //Calling the controller
        controllerLike();
    }
});