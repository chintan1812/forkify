import uniqid from 'uniqid'

export default class list {
    constructor(){
        this.items = [];
    }
    addItem (count, unit, ingredient) {
        const item ={
            id: uniqid(),
            count,
            unit,
            ingredient
        }
        this.items.push(item);
        return item;
    }

    deleteItem(id) {
        const index = this.items.findIndex(el => el.id === id);
        // [2,4,5] splice(1,1) --> returns 4, orginal array [2,5]
        // [2,4,5] slice(1,1) --> returns 4, orginal array [2,4,5]
        this.items.splice(index,1);
    }

    updateCount(id, newCount) {
        this.items.find(el => el.id === id).count = newCount;
    }
}