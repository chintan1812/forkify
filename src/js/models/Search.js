import axios from 'axios';

export default class Search {
    constructor(query){
        this.query = query;
    }
    async getResults(query) {
        try {
            const res = await axios(`https://forkify-api.herokuapp.com/api/search?&q=${this.query}`);
            this.result = res.data.recipes;
            // return recipe;
            // console.log(`This is the search result`);
            // console.log(this.result);
        } catch (error) {
            alert(error);
        }
    }
}